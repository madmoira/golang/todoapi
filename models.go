package main

import (
	"net/http"

	"github.com/go-chi/render"
)

type NoteInsert struct {
	ID             int64 `json:"id"`
	HTTPStatusCode int
}

func (n *NoteInsert) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, n.HTTPStatusCode)
	return nil
}

type NoteUpdate struct {
	Updated        int64 `json:"updated"`
	HTTPStatusCode int
}

func (n *NoteUpdate) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, n.HTTPStatusCode)
	return nil
}

type Note struct {
	ID         int    `db:"id" json:"id,omitempty"`
	Content    string `db:"content" json:"content"`
	IsComplete bool   `db:"is_complete" json:"is_complete"`
}

func (nr *Note) Bind(r *http.Request) error {
	nr.ID = 0
	return nil
}

func (n *Note) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func NoteResponse(n *Note) *Note {
	return n
}

func NotesResponse(nl []*Note) []render.Renderer {
	list := []render.Renderer{}
	for _, note := range nl {
		list = append(list, NoteResponse(note))
	}
	return list
}
