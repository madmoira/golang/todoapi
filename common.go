package main

import (
	"net/http"

	"github.com/go-chi/render"
)

type ErrResponse struct {
	HTTPStatusCode int
	StatusText     string
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrNotFound(msg string) render.Renderer {
	return &ErrResponse{
		HTTPStatusCode: 404,
		StatusText:     msg,
	}
}

func ErrGeneral(status int, msg string) render.Renderer {
	return &ErrResponse{
		HTTPStatusCode: status,
		StatusText:     msg,
	}
}
