-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE note (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  content TEXT,
  is_complete BOOLEAN
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE note;