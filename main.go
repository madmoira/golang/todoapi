package main

import (
	"fmt"
	"net/http"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

var db *sqlx.DB
var err error

func init() {
	db, err = sqlx.Open("sqlite3", "main.db")

	if err != nil {
		fmt.Printf("Error %v", err.Error())
	}

	err = db.Ping()

	if err != nil {
		fmt.Printf("Error %v", err.Error())
	}
}

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Get("/", getNotes)
	r.Get("/{noteID}", getNote)
	r.Post("/", postNote)
	r.Put("/{noteID}", putNote)
	r.Delete("/{noteID}", deleteNote)

	err := http.ListenAndServe(":3000", r)
	if err != nil {
		fmt.Printf("Error when starting server %v", err.Error())
	}
	fmt.Printf("Server started in %v", 3000)
}

func getNotes(w http.ResponseWriter, r *http.Request) {
	nl := []*Note{}

	err := db.Select(&nl, "SELECT * FROM note;")
	if err != nil {
		render.Render(w, r, ErrGeneral(200, "No notes found"))
		return
	}

	render.RenderList(w, r, NotesResponse(nl))
}

func getNote(w http.ResponseWriter, r *http.Request) {

	n := Note{}
	noteID := chi.URLParam(r, "noteID")

	err := db.Get(&n, "SELECT * FROM note WHERE id=$1;", noteID)

	if err != nil {
		render.Render(w, r, ErrNotFound("Note not found"))
		return
	}

	render.Render(w, r, NoteResponse(&n))
}

func postNote(w http.ResponseWriter, r *http.Request) {

	data := &Note{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrGeneral(400, "Error parsing data"))
		return
	}

	insertNote := "INSERT INTO note (content, is_complete) VALUES ($1, $2);"
	result, err := db.Exec(insertNote, data.Content, data.IsComplete)
	if err != nil {
		render.Render(w, r, ErrGeneral(500, "Error inserting data"))
		return
	}

	newNote, _ := result.LastInsertId()
	render.Render(w, r, &NoteInsert{ID: newNote, HTTPStatusCode: 201})
}

func putNote(w http.ResponseWriter, r *http.Request) {
	data := &Note{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrGeneral(400, "Error parsing data"))
	}

	n := Note{}
	noteID := chi.URLParam(r, "noteID")

	err := db.Get(&n, "SELECT * FROM note WHERE id=$1;", noteID)
	if err != nil {
		render.Render(w, r, ErrNotFound("Note not found"))
		return
	}

	result := db.MustExec(
		"UPDATE note SET content=$1, is_complete=$2 WHERE id=$3",
		data.Content,
		data.IsComplete,
		n.ID,
	)

	updated, _ := result.RowsAffected()
	render.Render(w, r, &NoteUpdate{Updated: updated, HTTPStatusCode: 200})
}

func deleteNote(w http.ResponseWriter, r *http.Request) {

	noteID := chi.URLParam(r, "noteID")

	n := Note{}
	err := db.Get(&n, "SELECT * FROM note WHERE id=$1;", noteID)
	if err != nil {
		render.Render(w, r, ErrNotFound("Note not found"))
		return
	}

	result := db.MustExec(
		"DELETE FROM note WHERE id=$1;",
		noteID,
	)

	updated, _ := result.RowsAffected()
	render.Render(w, r, &NoteUpdate{Updated: updated, HTTPStatusCode: 200})
}
